import os
import sys

dir = "."
if (len(sys.argv) == 2):
	dir = sys.argv[1]
	
for file in os.listdir(dir):
	if(file.endswith(".in")):
		name = os.path.splitext(file)[0]
		out = name+".out"
		png = name+".png"
		cmd = "python graphsol.py "+dir+"/"+file+" "+dir+"/"+out+" "+dir+"/"+png
		print(cmd)
		os.system(cmd)
