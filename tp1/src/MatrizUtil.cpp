#include <iostream>
#include "MatrizUtil.h"
using namespace std;


Matriz<double> buildMatrizNula(unsigned height, unsigned width){
      Matriz<double> matrizNula = Matriz<double>(height,width);
      return matrizNula;
    }


// m = cant columnas (a/h +1)
// n = cant filas   (b/h +1)
Matriz<double> buildMatrizBandaCompacta(int m, int n, double h, list<Sanguijuela>& sanguijuelas,Matriz<double>& b, unsigned int sanguijuelaIgnorada) {
    int nm = n*m;

    Matriz<double> M = buildMatrizNula(m*2 +1 ,nm);
    for(int k = 0; k < nm; k++){
         //M[m][] es la diagonal principal. Los valores de otras diagonales se ponen en referencia a esta
         //b[i] se corresponde con la fila que pasa por M[i][i]

        //esBorde = es primera fila || es primer col de nueva fila || es ultima col de esta fila || es ultima fila;
        bool esBorde = k<m || k%m == m-1 || k%m == 0 || k >= nm-m;

        //Hay sanguijuela si dadas las coordenadas x,y para un punto discreto afectado por la sanguijuela
        // cuando k = x * m + y
        // Sería mejor saber que sanguijuela existe en este punto.
        // Podria ser con una clase Sanguijuela que guarde los puntos afectados y su temp

        //bool haySanguijuela = m_sanguijuelas[k/m][k%m] != sanguijuelas.end();
        list<Sanguijuela>::iterator sanguijuela = getSanguijuela(k%m,k/m,sanguijuelas,sanguijuelaIgnorada);/// k%m es la columna, es el x   ///  k/m es la fila es el y
        bool haySanguijuela = sanguijuela != sanguijuelas.end() ;

        if(esBorde){
            M[m][k] = 1.0;
            b[k][0] = -100.0;
        }else if(haySanguijuela){
            M[m][k] = 1.0;
            b[k][0] = sanguijuela->t;
        }else{
            // Si no es borde ni sanguijuela tenemos que calcular la temp.
            M[m][k] = -4.0;
            if(k > 0){
                M[m+1][k-1] = 1.0;
            }
            if( k < nm-1){
                M[m-1][k+1] = 1.0;
            }
            if( k > m){
                M[2*m][k-m] = 1.0;
            }
            if( k+m < nm){
                M[0][k+m] = 1.0;
            }
            b[k][0] = 0;
        }
    }
    return M;
}





