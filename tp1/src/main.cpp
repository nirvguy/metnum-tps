#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sys/time.h>
#include "util.h"
#include "sanguijuelas.h"
#include "MatrizUtil.h"
#include "ResolucionUtil.h"

// #define DEBUG

using namespace std;


timeval start, endTime;
//////////////////////////////////////////////////////////////
void init_time()
{
      gettimeofday(&start,NULL);
}
//////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////
double get_time()
{
      gettimeofday(&endTime,NULL);
      return
(1000000*(endTime.tv_sec-start.tv_sec)+(endTime.tv_usec-start.tv_usec))/1000000.0;
}
//////////////////////////////////////////////////////////////

enum EstadoParsing { ESTADO_INICIO, ESTADO_SANGUIJUELAS };
enum Metodo { GAUSS = 0,
			  LU = 1,
			  ELIMINAR_SANGUIJUELA = 2,
			  ELIMINAR_SANGUIJUELA_SHERMAN_MORRISON = 3,
			  VARIACION_TEMP_COMPARACION = 4};

#define MAX_H 50

void usage() {
	cout << "./sanguijuelas <input_filename> <output_filename> <metodo>" << endl;
}





int main(int argc, char *argv[]) {
	if(argc != 4) {
		usage();
		return 0;
	}
	ifstream entrada(argv[1],std::ifstream::in);
	ofstream salida(argv[2],std::ofstream::out);
	Metodo metodo = (Metodo) string_to_type<int>(argv[3]);



    unsigned int num_linea = 1;
    int estado = 0;

    double a = 0;
	double b = 0;
    double h = 0;
    unsigned int k = 0;
    unsigned int indiceSanguijuela = 0;
    list<Sanguijuela> sanguijuelas;

    for (string linea; getline(entrada, linea); num_linea++) {
        vector<string> valores = separar(linea);

        switch (estado) {
            case ESTADO_INICIO:
                if (valores.size() != 4) {
                    throw runtime_error("Se esperaban 4 valores en línea " + \
                            type_to_string<int>(num_linea));
                }
                a = string_to_type<double>(valores[0]);
                b = string_to_type<double>(valores[1]);
                h = string_to_type<double>(valores[2]);
                // k cant de sanguijuelas
                k = string_to_type<int>(valores[3]);
                estado = ESTADO_SANGUIJUELAS;
                break;
            case ESTADO_SANGUIJUELAS: {
                if (valores.size() != 4) {
                    throw runtime_error("Se esperaban 4 valores en línea " + \
                            type_to_string<int>(num_linea));
                }
                list<PuntoSanguijuela> puntosSanguijuela;
				Sanguijuela sanguijuela = {
				    indiceSanguijuela,
					string_to_type<double>(valores[0]),
					string_to_type<double>(valores[1]),
					string_to_type<double>(valores[2]),
					string_to_type<double>(valores[3]),
					puntosSanguijuela
				};
				sanguijuelas.push_back(sanguijuela);
				indiceSanguijuela++;


				if(k==indiceSanguijuela) {


                    discretizarSanguijuelas(sanguijuelas,a,b,h);

                    // b es el alto del parabrisas, b = n * h
                    // a es el ancho del parabrisas, a = m * h
                    // Con m y n naturales.
                    unsigned int cantidadFilasP= b/h +1;
                    unsigned int cantidadColumnasP= a/h + 1;
                    int longitud=cantidadColumnasP*cantidadFilasP;

                    Matriz<double> terminoIndep(longitud,1);
                    init_time();

					switch(metodo) {
						case GAUSS: {
                            cout << "Construyendo la matriz del sistema (compacta)..." <<endl;
                            Matriz<double> matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep,-1);
                            cout << "Matriz construida." <<endl;

                            cout << "Eliminacion gaussiana sobre la matriz compacta..." <<endl;
                            gaussSobreCompacta(matrizBanda,terminoIndep,false);
                            cout << "Termino EG sobre la matriz compacta." <<endl;

                            cout << "Resolviendo triangular superior compacta..." <<endl;
                            Matriz<double> resGaussCompacto =resolverTriangularSuperiorCompacta(matrizBanda, terminoIndep);
                            cout << "Resuelta" << endl;

                            cout << "Temp en punto critico = "<<temperaturaEnPuntoCritico(resGaussCompacto,a,b,h)<<endl;

                            cout << "Tiempo de ejecucion: "<<get_time()<<" s"<<endl;

                            cout << "Imprimiendo a "<< argv[2] <<endl;
                            for(int i=0;i<longitud;i++){
                                 salida << std::fixed << std::setprecision(5) << i/cantidadColumnasP<<"\t"<<i%cantidadColumnasP<<"\t"<<resGaussCompacto[i][0] <<endl;
                            }

							break;
						}
						case LU: {
                            cout << "Construyendo la matriz del sistema (compacta)..." <<endl;
                            Matriz<double> matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep,-1);
                            cout << "Matriz construida." <<endl;

                            gaussSobreCompacta(matrizBanda,terminoIndep,true);

                            // Ax = b ; A = LU ; Lz = b ; Ux = z
                            cout << "Resolviendo Lz = b" <<endl;
                            Matriz<double> z = resolverTriangularInferiorCompactaLU(matrizBanda, terminoIndep);
                            cout << "Resolviendo Ux = z" <<endl;
                            Matriz<double> x = resolverTriangularSuperiorCompacta(matrizBanda, z);


                            cout << "Temp en punto critico = "<<temperaturaEnPuntoCritico(x,a,b,h)<<endl;

                            cout << "Tiempo de ejecucion: "<<get_time()<<" s"<<endl;


						    cout << "Imprimiendo a "<< argv[2] <<endl;
                            for(int i=0;i<longitud;i++){
                                 salida << std::fixed << std::setprecision(5) << i/cantidadColumnasP<<"\t"<<i%cantidadColumnasP<<"\t"<<x[i][0] <<endl;
                            }
						    break;
						}
						case ELIMINAR_SANGUIJUELA: {
						    unsigned int sanguijuelaEliminada = 0;

                            Matriz<double> matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep,-1);
                            gaussSobreCompacta(matrizBanda,terminoIndep,false);
                            Matriz<double> x =resolverTriangularSuperiorCompacta(matrizBanda, terminoIndep);
                            double temp = temperaturaEnPuntoCritico(x,a,b,h);


                            cout << "Sin remover sanguijuelas, la temperatura en el punto critico es "<<temp<<endl;

                            // k cant de sanguijuelas
                            for(unsigned int i = 0; i<k; i++){
                                double tAntes=get_time();
                                matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep, i);
                                gaussSobreCompacta(matrizBanda,terminoIndep,false);
                                Matriz<double> x2 =resolverTriangularSuperiorCompacta(matrizBanda, terminoIndep);
                                double newTemp = temperaturaEnPuntoCritico(x2,a,b,h);
                                cout << "Eliminando sanguijuela "<<i<<", temp="<<newTemp<<endl;
                                if(newTemp < temp){
                                    temp = newTemp;
                                    sanguijuelaEliminada = i;
                                }

                                cout << "Tiempo de consulta una sanguijuela: "<<get_time() - tAntes<<" s"<<endl;
                            }

                            if(temp < 235){
                                cout << "El parabrisas resiste eliminando la sanguijuela "<<sanguijuelaEliminada<<"."<< " (temp="<<temp<<")"<<endl;
                            }else{
                                cout << "El parabrisas se rompe, aun eliminando la mejor sanguijuela ("<<sanguijuelaEliminada<<", temp="<<temp<<")"<<endl;
                            }

                            cout << "Tiempo de ejecucion: "<<get_time()<<" s"<<endl;

                            cout << "Imprimiendo a "<< argv[2] <<endl;
                            for(int i=0;i<longitud;i++){
                                 salida << std::fixed << std::setprecision(5) << i/cantidadColumnasP<<"\t"<<i%cantidadColumnasP<<"\t"<<x[i][0] <<endl;
                            }


						    break;
						}
						case ELIMINAR_SANGUIJUELA_SHERMAN_MORRISON: {
						    unsigned int sanguijuelaEliminada = 0;

                            Matriz<double> matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep,-1);
                            gaussSobreCompacta(matrizBanda,terminoIndep,true);
                            Matriz<double> x1 = resolverLU(matrizBanda,terminoIndep);
                            Matriz<double> x2 (x1.filas(),1);
                            double temp = temperaturaEnPuntoCritico(x1,a,b,h);


                            cout << "Sin remover sanguijuelas, la temperatura en el punto critico es "<<temp<<endl;


                            for(list<Sanguijuela>::iterator it=sanguijuelas.begin();it!=sanguijuelas.end();it++) {
                                double tAntes=get_time();
                                if(it->puntos.size() == 1){
                                    list<PuntoSanguijuela>::iterator punto = it->puntos.begin();
                                    //Si ocupa solo un punto y es borde, la ignoramos,
                                    // eliminarla no cambia el sistema original
                                    if(punto->c != 0 && punto->c != cantidadColumnasP-1
                                            && punto->f != 0 && punto->f != cantidadFilasP-1){
                                        cout << "Sanguijuela "<<it->i<<" se resuelve por S-M"<<endl;
                                        list<Sanguijuela>::iterator sanguijuela = getSanguijuela(punto->c, punto->f,sanguijuelas,it->i);
                                        int indiceSistema = punto->c + (punto->f * cantidadColumnasP) ;

                                        if(sanguijuela != sanguijuelas.end()){
                                            // Si hay otra sanguijuela en el punto, lo unico que cambia es el termino indep.
                                            Matriz<double> modTerminoIndep(longitud,1);
                                            modTerminoIndep[indiceSistema][0] = sanguijuela->t - terminoIndep[indiceSistema][0];
                                            modTerminoIndep = terminoIndep + modTerminoIndep;
                                            resolverLU(matrizBanda, modTerminoIndep);

                                        }else{

                                            Matriz<double> u(longitud,1);
                                            u[indiceSistema][0] = 1;

                                            Matriz<double> vt(1,longitud);
                                            vt[0][indiceSistema] = -5;
                                            vt[0][indiceSistema+1] = 1;
                                            vt[0][indiceSistema-1] = 1;
                                            vt[0][indiceSistema+cantidadColumnasP] = 1;
                                            vt[0][indiceSistema-cantidadColumnasP] = 1;

                                            x2 = resolucionShermanMorrison(matrizBanda,u,vt,x1,it->t);

                                        }
                                    }
                                }else{
                                    Matriz<double> terminoIndepElim(longitud,1);
                                    Matriz<double> matrizBandaElim = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndepElim,it->i);
                                    gaussSobreCompacta(matrizBandaElim,terminoIndepElim,false);
                                    x2 =resolverTriangularSuperiorCompacta(matrizBandaElim, terminoIndepElim);
                                }

                                double newTemp = temperaturaEnPuntoCritico(x2,a,b,h);
                                cout << "Eliminando sanguijuela "<<it->i<<", temp="<<newTemp<<endl;
                                if(newTemp < temp){
                                    temp = newTemp;
                                    sanguijuelaEliminada = it->i;
                                }
                                cout << "Tiempo de consulta una sanguijuela: "<<get_time() - tAntes<<" s"<<endl;

                            }

                            if(temp < 235){
                                cout << "El parabrisas resiste eliminando la sanguijuela "<<sanguijuelaEliminada<<"."<< " (temp="<<temp<<")"<<endl;
                            }else{
                                cout << "El parabrisas se rompe, aun eliminando la mejor sanguijuela ("<<sanguijuelaEliminada<<", temp="<<temp<<")"<<endl;
                            }

                            cout << "Tiempo de ejecucion: "<<get_time()<<" s"<<endl;

                             cout << "Imprimiendo a "<< argv[2] <<endl;
                            for(int i=0;i<longitud;i++){
                                 salida << std::fixed << std::setprecision(5) << i/cantidadColumnasP<<"\t"<<i%cantidadColumnasP<<"\t"<<x1[i][0] <<endl;
                            }


						    break;
						}
						case VARIACION_TEMP_COMPARACION: {
							int iteraciones = 10;
							cout << "Temperatura disminuyendo un 50% cada iteracion, "<<iteraciones<<" iteraciones"<<endl;

							cout << "Resolucion por fact. LU ";
							init_time();
                            Matriz<double> matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep,-1);
                            gaussSobreCompacta(matrizBanda,terminoIndep,true);
                            cout << "Resolviendo LUx = b0" <<endl;
                            Matriz<double> x = resolverLU(matrizBanda, terminoIndep);



                            for(int i = 1;i<iteraciones;i++){
                                for(int j = 0;j<terminoIndep.filas();j++){
                                    if(terminoIndep[j][0] != -100){
                                        terminoIndep[j][0] = 0.5 * terminoIndep[j][0] ;
                                    }
                                }
                                cout << "Resolviendo LUx = b"<<i<<endl;
                                x = resolverLU(matrizBanda, terminoIndep);
                            }
                            cout << "Tiempo de computo con fact. LU: "<<get_time()<<endl;


                            cout << "Resolucion por EG ";
							init_time();
							cout << "Resolviendo Ax = b0" <<endl;
                            matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndep,-1);
                            gaussSobreCompacta(matrizBanda,terminoIndep,false);
                            x = resolverTriangularSuperiorCompacta(matrizBanda, terminoIndep);

                            for(int i = 1;i<iteraciones;i++){
                                cout << "Resolviendo Ax = b"<<i <<endl;
                                Matriz<double> terminoIndepIt(longitud,1);
                                matrizBanda = buildMatrizBandaCompacta(cantidadColumnasP, cantidadFilasP, h, sanguijuelas,terminoIndepIt,-1);
                                for(int j = 0;j<terminoIndepIt.filas();j++){
                                    if(terminoIndepIt[j][0] != -100){
                                        terminoIndepIt[j][0] = 0.5 * terminoIndepIt[j][0]  ;
                                    }
                                }
                                gaussSobreCompacta(matrizBanda,terminoIndepIt,false);
                                x = resolverTriangularSuperiorCompacta(matrizBanda, terminoIndepIt);
                            }
                            cout << "Tiempo de computo con EG: "<<get_time()<<endl;

						    break;
						}
						default:
							throw runtime_error("No existe ese metodo");
					}


                    a = b = h = 0;
                    sanguijuelas.clear();


                    estado = ESTADO_INICIO;
                }
                break;
			}
            default:
                throw runtime_error("Estado " + type_to_string<int>(estado) + \
                        " inesperado después de línea " + type_to_string<int>(num_linea));
                break;
        }
    }


    return 0;
}
