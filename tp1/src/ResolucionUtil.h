#ifndef RESOLUCIONUTIL_H_INCLUDED
#define RESOLUCIONUTIL_H_INCLUDED

#include "Matriz.h"

void gaussSobreCompacta(Matriz<double> & M, Matriz<double>& b, bool factLU);//longitud es la cantidad de columnas de la matriz, ancho es el ancho de la matriz banda, en este caso

Matriz<double> resolverTriangularSuperiorCompacta(Matriz<double> & M, Matriz<double>& b);
Matriz<double> resolverTriangularInferiorCompactaLU(Matriz<double>& M, Matriz<double>& b);

Matriz<double> resolucionShermanMorrison(Matriz<double>& mLU, Matriz<double>& u, Matriz<double>& vt, Matriz<double>& x1, double ts );

Matriz<double> resolverLU(Matriz<double>& mLU, Matriz<double>& terminoIndep);

double temperaturaEnPuntoCritico(Matriz<double>& arregloDeTemperaturas, double a, double b, double h);

#endif // RESOLUCIONUTIL_H_INCLUDED
