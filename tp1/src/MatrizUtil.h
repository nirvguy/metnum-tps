#ifndef MATRIZUTIL_H_INCLUDED
#define MATRIZUTIL_H_INCLUDED

#include <list>
#include "tipos.h"
#include "Matriz.h"
#include "sanguijuelas.h"

using namespace std;

Matriz<double> buildMatrizNula(unsigned height, unsigned width);
Matriz<double> buildMatrizBandaCompacta(int m, int n, double h, list<Sanguijuela>& sanguijuelas, Matriz<double>&  b,unsigned int sanguijuelaIgnorada);

#endif // MATRIZUTIL_H_INCLUDED
