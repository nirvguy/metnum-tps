#ifndef TIPOS_H

#define TIPOS_H
#include <vector>
#include <list>

using namespace std;

struct PuntoSanguijuela {
    bool operator==(const PuntoSanguijuela p){
        return c == p.c && f == p.f;
    }
	unsigned int c;/// c=colummna(asociada a x)
	unsigned int f;/// f=fila(asociada a y),
};

struct Sanguijuela {
    // i indice de sanguijuela
    unsigned int i;
    // x,y coordenadas en el parabrisas continuo
	double x,y;
	// r radio, t temperatura
	double r,t;

	list<PuntoSanguijuela> puntos;
};



typedef vector< vector< list<Sanguijuela>::iterator > > Matriz_Sanguijuelas;

#endif /* end of include guard: TIPOS_H */
