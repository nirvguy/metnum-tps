#ifndef SANGUIJUELAS_H
#define SANGUIJUELAS_H

#include <vector>
#include <list>
#include <cmath>
#include "tipos.h"

using namespace std;


inline bool intersec_circulo(double p_x,double p_y,double c_x,double c_y,double r);

void discretizarSanguijuelas(list<Sanguijuela>& sanguijuelas,double a, double b, double h);

list<Sanguijuela>::iterator getSanguijuela (unsigned int x, unsigned int y, list<Sanguijuela>& sanguijuelas,unsigned int sanguijuelaIgnorada);

#endif /* end of include guard: SANGUIJUELAS_H */
