#include "ResolucionUtil.h"
#include <iostream>
using namespace std;

/**
*   La resolución de sistemas triangulares por back-subst. para Ax=b es:
*   x[i] = (b[i] - resta) / A[i][i]
*
*   Con resta = Sumatoria con j = i+1..n de M[i][j] * x[j]
*
*
*   La matriz banda compacta (Mc) se almacena en una matriz de (2*m)+1 Filas y m*n Columnas
*   Con m = cant de Columnas (discretas) y n cant de Filas (discretas) del parabrisas.
*   La fila Mc[m][k], con k=0..(m*n) es la diagonal principal de la matriz original (M).
*   Una vez triangulada Mc con EG, las filas debajo de m (con indice > m) son nulas.
*   Las filas de la matriz original estan dispuestas en contradiagonales a partir
*   de la diag. ppal.
*
*   Ej:
*   [   *   *   *   *   *       *         M[n-1][n] ]
*   [   *   *   *   *   *   M[n-1][n-1]   M[n][n]   ]
*   [   0   0   0   0   0       0           0       ]
*
*
*/
Matriz<double> resolverTriangularSuperiorCompacta(Matriz<double>& M,Matriz<double>& b){

    int longitud = M.columnas();
    int ancho =  M.filas()/2;
    Matriz<double> res(longitud,1);
    // A partir de la ultima columna
    for(int i=longitud-1;i>=0;i--){
        double sumaArestar=0.0;

        // Se suman los productos de coeficiente y resultados calculados
        // desde la esquina superior de la contradiagonal (es decir, la fila original)
        for(int k=ancho;k>=1;k--){
            // Guarda para los primeros m (ancho) resultados, en que la contradiagonal
            // se corta antes del ancho de la banda
            if(i+k<=longitud-1) {
                    sumaArestar = sumaArestar + res[i+k][0] * M[ancho-k][i+k];
            }
        }

        res[i][0]=( ( double)(b[i][0] - sumaArestar))/(( double)M[ancho][i]);
    }

    return res;

}


/**
*   Esta funcion asume que esta tratando con una triang. inf.
*   con unos en la diagonal (como L de una fact LU)
*/
Matriz<double> resolverTriangularInferiorCompactaLU(Matriz<double>& M,Matriz<double>& b){
    int longitud = M.columnas();
    int ancho =  M.filas()/2;
    Matriz<double> res(longitud,1);
    // A partir de la primer
    for(int i=0; i<longitud; i++){
        double sumaArestar=0.0;

        // Se suman los productos de coeficiente y resultados calculados
        // desde la esquina inferior de la contradiagonal (es decir, la fila original)
        for(int k=1;k<=ancho;k++){
            // Guarda para los primeros m (ancho) resultados, en que la contradiagonal
            // se corta antes del ancho de la banda
            if(i-k>=0) {
                    sumaArestar = sumaArestar + res[i-k][0] * M[ancho+k][i-k];
            }
        }
        // L tiene 1s en la diagonal (M[ancho][i] = 1)
        res[i][0]=( ( double)(b[i][0] - sumaArestar));
    }

    return res;

}



/**
* Al hacer gauss, calculamos tambien la factorizacion LU.
* Aprovechamos el espacio debajo de la diagonal principal, que por EG
* sabemos que son 0, para guardar L. U es la triangular superior
* resultante de hacer EG.
* Cuando factorizamos en LU no se modifica el termino indep.
*
* Como la fact. LU de matrices banda conserva el ancho de la banda,
* es decir, la misma cant. de diagonales a ambos lados de la diag. ppal.,
* podemos almacenar L y U en una matriz de las mismas dimensiones que M
*/
void gaussSobreCompacta(Matriz<double>& M, Matriz<double>& b, bool factLU){
    int ancho = M.filas()/2;
    int longitud = M.columnas();

    for(int i=0;i<=longitud-2;i++){

        int f=i+1;
        for(int j=ancho+1;j<=2*ancho;j++){

            double multiplicador = (((double)M[j][i])/((double)M[ancho][i]));

            if(f <= longitud-1){
                 // Con k = 0, M[j][i] = 0
                for (int k = ancho; k>0; k--){
                   if(i+k<=longitud-1){
                        M[j-k][i+k]=M[j-k][i+k] - multiplicador * M[ancho-k][i+k];
                   }
                }

                // En M[j][i] guardamos multiplicador para hacer fact. LU
                if(factLU){
                    M[j][i] = multiplicador;
                }else{
                    M[j][i] = 0.0;
                    b[f][0] = b[f][0] - multiplicador * b[i][0];
                }
                f++;
            }
        }
    }
}


double temperaturaEnPuntoCritico(Matriz<double>& arregloDeTemperaturas, double a, double b, double h){
    double pcContinuoX = a/2;
    double pcContinuoY = b/2;
    int filas = b/h;
    int cols = a/h;

    // El pc continuo esta entre x1 y x2 del espacio discreto
    // Si a/2 cae en un punto discreto, x1 = x2
    int x1 = pcContinuoX/h;
    int x2 = x1 + (pcContinuoX - x1*h > 0 && x1 < cols-1 ? 1 : 0);

    // El pc continuo esta entre y1 y y2 del espacio discreto
    // Si b/2 cae en un punto discreto, y1 = y2
    int y1 = pcContinuoY/h;
    int y2 = y1 + (pcContinuoY - y1*h > 0 && y1 < filas-1 ? 1 : 0);


    // Tomamos el promedio entre los 4 puntos
    // Esto vale aun cuando algun coeficiente coincide con su punto discreto
    double pcTemp= (arregloDeTemperaturas[y1*(cols+1) + x1][0] +
                          arregloDeTemperaturas[y1*(cols+1) + x2][0] +
                          arregloDeTemperaturas[y2*(cols+1) + x1][0] +
                          arregloDeTemperaturas[y2*(cols+1) + x2][0]) / 4;

    return pcTemp;
}


Matriz<double> resolverLU(Matriz<double>& mLU, Matriz<double>& terminoIndep){
    // Ax = b ; A = LU ; Lz = b ; Ux = z

    Matriz<double> z = resolverTriangularInferiorCompactaLU(mLU, terminoIndep);
    Matriz<double> x = resolverTriangularSuperiorCompacta(mLU, z);
    return x;
}

Matriz<double> resolucionShermanMorrison(Matriz<double>& mLU, Matriz<double>& u, Matriz<double>& vt,
                    Matriz<double>& x1, double ts ){
    // ts temperatura de sanguijuela eliminada
    // Ay = u => y = A^-1 * u
    // x1 = (A^-1 * b1)
    // b2 = (b1 - ts * u)
    // x2 = (A + u * vt)^-1 * b2
    // x2 =  x1 - ts * y - (y * ( vt * (x1 - ts * y)) / (1 + vt * y)

    Matriz<double> y = resolverLU(mLU,u);
    Matriz<double> vty = vt * y; // vt: 1xn, y: nx1, vty = 1x1
    double div = 1/(1+ vty[0][0]);
	Matriz<double> yts = y*ts;

	// Ab2 = x1 - ts * y ,Ab2: nx1
	Matriz<double> Ab2 = x1-yts;

	// vtb2 = vt * Ab2, vtb2 : 1x1
	Matriz<double> vtb2 = vt*Ab2;
	Matriz<double> prod = y * (vtb2[0][0] * div);

	// x2 =  Ab2 - ( y *  vtb2 / (1 + vt * y) )
    Matriz<double> x2 =  Ab2 - prod;

    return x2;
}

