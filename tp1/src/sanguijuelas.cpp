#include "sanguijuelas.h"
#include <algorithm>

inline bool intersec_circulo(double p_x,double p_y,double c_x,double c_y,double r) {
	return (p_x-c_x)*(p_x-c_x) + (p_y-c_y)*(p_y-c_y) <= r*r;
}

void discretizarSanguijuelas(list<Sanguijuela>& sanguijuelas,double a, double b, double h) {
	unsigned int m=((unsigned int) (a/h))+1; // Cantidad de columnas
	unsigned int n=((unsigned int) (b/h))+1; // Cantidad de filas
	for(unsigned int i=0;i<n;i++){
        for(unsigned int j=0;j<m;j++) {
			double x = ((double) j) * h;
			double y = ((double) i) * h;
			for(list<Sanguijuela>::iterator it=sanguijuelas.begin();it!=sanguijuelas.end();it++) {
                if(intersec_circulo(x,y,it->x,it->y,it->r)) {
                    PuntoSanguijuela punto = {j,i};/// c=j(es la columna) f=i (es la fila)
                    (it->puntos).push_back(punto);
                }
			}
		}
	}
}

/**
* Dadas coordenadas en el parabrisas discreto, se busca la sanguijuela de mayor temperatura
* que abarca ese punto, ignorando la sanguijuela de indice indicado
*/
list<Sanguijuela>::iterator getSanguijuela (unsigned int x, unsigned int y, list<Sanguijuela>& sanguijuelas,unsigned int sanguijuelaIgnorada){
    list<Sanguijuela>::iterator sanguijuela = sanguijuelas.end();

    for(list<Sanguijuela>::iterator it=sanguijuelas.begin();it!=sanguijuelas.end();it++) {

        if(it->i != sanguijuelaIgnorada){

            for(list<PuntoSanguijuela>::iterator punto =it->puntos.begin(); punto!=it->puntos.end(); punto++) {

                if( punto->c == x && punto->f == y && (sanguijuela == sanguijuelas.end() || (sanguijuela->t) < (it->t))){
                    sanguijuela = it;
                }
            }
        }
    }

    return sanguijuela;

}

