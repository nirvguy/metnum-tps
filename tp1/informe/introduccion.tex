\section{Introducción teórica}

Dado que el modelado del problema presentado en este trabajo se realiza mediante un sistema de ecuaciones lineales y para su resolución utilizamos distintos algoritmos que permiten la resolución de dicho sistema, a continuación daremos un breve detalle de sus fundamentos teóricos.

\subsection{Sistemas de ecuaciones lineales}

Dado un sistema de $n$ ecuaciones lineales con $x_1,\ldots,x_n$ incógnitas y las constantes $a_{ij}$ para cada $i, j = 1, 2,\ldots,n$ y $b_i$, para cada $i = 1,2,\ldots,n$, podemos representarlo de forma matricial a través de la matriz $A \in \mathbb{R}^{n \times n} / A = [a_{ij}] $, $ x \in \mathbb{R}^{n} / x = [x_i]$ y $b \in \mathbb{R}^{n} / b = [b_i]$, como el sistema definido por la transformación lineal $Ax=b$. 

Para simplificar el sistema se pueden realizar ciertas operaciones entre sus ecuaciones, o análogamente sobre filas de la matriz $A$ aumentada con el vector $b$. Las filas pueden ser sumadas, multiplicadas por constantes distintas de cero e intercambiadas entre si. Como resultado se obtiene un sistema equivalente, es decir, conserva el conjunto de soluciones del sistema original.\cite{BurdenSisEq}


\subsection{Eliminación Gaussiana}

El método de eliminación gaussiana es un algoritmo para triangular un sistema de ecuaciones lineales, con el fin de obtener un sistema equivalente con ceros debajo de la diagonal. 
Consiste en realizar una serie de operaciones de filas sobre la matriz asociada al sistema aumentada con el vector de resultados.\\
En el sistema descrito en la sección anterior: dado $a_{ii} \neq 0$, para $i = 2, 3, \ldots, n - 1$, se realiza la operación $
(E_j - (\frac{a_{ji}}{a_{ii}})E_i) \rightarrow (E_j)
$ para cada $j = i + 1, i + 2, \ldots , n$. En caso de que $a_{ii} = 0$ se hace un intercambio con una fila $ j $ con $ j>i $ y $a_{ji} \neq 0$, y se continua con el proceso. Si no existe una fila con la anterior condición, entonces no se puede hacer el intercambio de filas. En esta situación el sistema de ecuaciones no tiene una solución única, y puede no tener infinitas o ninguna solución.
El algoritmo tiene complejidad $O(n^3)$. \cite{BurdenGauss}

Una vez obtenido el sistema triangular superior equivalente es posible resolverlo mediante sustitución hacia atrás, donde cada $x_i$ se obtiene de la siguiente forma \cite{BurdenBackwards}: 

$$
x_i = 
\frac
    {b_{i} - \sum_{j= i + 1}^{n} a_{ij}x_j}
    {a_{ii}}
$$ 

Este algoritmo para resolver el sistema tiene una complejidad $O(n^2)$.


\subsection{Matriz banda}

Una matriz de $n \times n$ se la llama matriz banda si existen enteros $p$ y $q$, con $1 < p,q < n$, tales que $a_{ij} = 0$ siempre que $i + p \leq j$ o $j + q \leq i$. El ancho de banda de las matrices banda se define como $w = p + q - 1$. 
Estas matrices concentran todos sus elementos distintos de cero alrededor de la diagonal.

Dada la estructura particular de este tipo de matrices las formas de almacenamiento y los algoritmos de factorización pueden ser simplificados considerablemente por la gran cantidad de ceros que aparecen con patrones regulares.

\subsection{Factorización LU}

Dado un sistema de ecuaciones lineales $Ax = b$, si la eliminación gaussiana puede ser aplicada al sistema sin intercambiar filas, entonces la matriz $A$ puede ser factorizada en forma $A = LU$ donde $L$ es una matriz triangular inferior con unos en su diagonal y $U$ una matriz triangular superior. 
Además pueden tomarse como $U$ a la matriz triangular superior que resulta de aplicar la eliminación gaussiana sobre $A$, y como $ L $ una matriz formada por los multiplicadores $  \lambda $ usados en gauss en cada paso de la forma $(E_i)  \leftarrow (E_i + \lambda{}E_j) $. \cite{BurdenLU}

Si $A = LU$, entonces se puede resolver más fácilmente el sistema mediante un proceso de 2 pasos.\\
Primero tomamos $y = Ux$ y resolvemos el sistema $Ly = b$ para $y$. Como $L$ es triangular inferior, resolver este sistema puede hacerse directamente mediante sustitución hacia adelante. Este procedimiento es análogo a la sustitución hacia atrás mencionada en la sección anterior, por lo que también tiene una complejidad de $O(n^2)$. 
Una vez que $y$ es conocido, resolvemos el sistema $Ux = y$ mediante sustitución hacia atrás.\\
Por lo tanto, si bien determinar las matrices $L$ y $U$ tiene una complejidad de $O(n^3/3)$, una vez determinada la factorización, resolver un sistema que involucre a la matriz $A$ con cualquier vector $b$ puede ser resuelto con complejidad $O(2n^2)$.

\subsection{Formula de Sherman-Morrison}

Dada una matriz $A$ de $n \times n$ en la cual ya tengamos calculada su inversa $(A^{-1})$, si cambiamos algunos elementos de $A$ obteniendo así $A'$, podemos calcular la inversa de $A'$ de manera más eficiente que si aplicáramos un algoritmo general para invertir matrices. La formula de \textbf{Sherman-Morrison} (figura \ref{eq:shermanmorrison}) toma en consideración que ya teníamos calculada $A^-1$ para calcular $A'^{-1}$.\\
\begin{figure}[H]
\begin{equation}
A'^{-1} = (A+uv^t)^{-1} = A^{-1} - \dfrac{(A^{-1}u)(v^tA^{-1})}{1+v^tA^{-1}u}
\end{equation}
\caption{Formula de Sherman-Morrison}
\label{eq:shermanmorrison} 
\end{figure}

En la formula de la figura \ref{eq:shermanmorrison}, $A$ es una matriz cuadrada de $n \times n$ cuya inversa sabemos, $u$ y $v$ son las columnas de altura $n$ que definen la matriz modificante de $A$ ($u_{i}v_{j}$ son sumados a $a_{ij}$).
Dependiendo de $u$ y $v$, hay distintos tipos de modificaciones en la matriz $A$:

\begin{enumerate}
\item Si solo $u_i$ y $v_i$ son distintos de cero, entonces $u_iv_j$ es sumado a $a_{ij}$
\item Si $u_i =1$ y los otros componentes del vector son cero, entonces el vector $v$ es sumado a la fila $i$ de $A$
\item Si $v_j =1$ y los otros componentes del vector son cero, entonces el vector $u$ es sumado a la columna $j$ de $A$
\item Si $u$ y $v$ son vectores arbitrarios, entonces la fila $v$ y columna $u$ son sumados a diferentes filas y columnas de $A$ con diferente multiplicadores.
\end{enumerate}


